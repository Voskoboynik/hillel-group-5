from django.contrib import admin
from core.models import Group, Student, Teacher, Logger, ContactUs, ExchangeCurrency  # , MyNewUser


class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'course')
    list_display_links = ('id', 'course')
    search_fields = ('course',)
    list_filter = ('group_lead',)


class TeacherAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email')
    list_display_links = ('id', 'name')
    search_fields = ('name',)


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'age', 'email')
    list_display_links = ('id', 'name')
    search_fields = ('name',)
    list_filter = ('course',)


# Register your models in admin here.
admin.site.register(Group, GroupAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(ContactUs)
admin.site.register(Logger)
admin.site.register(ExchangeCurrency)
# admin.site.register(MyNewUser)  # if got abstractuser
