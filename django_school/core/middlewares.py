# import MiddlewareMixin class for making custom middlewares:
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect  # will be required for global login
from core.models import Logger
from django.utils import timezone
import time


# made middleware for writing parameters in Logger Model:
class LogMiddleware(MiddlewareMixin):

    # exclude_urls = [
    #     '/admin/'
    # ]

    def process_request(self, request):
            self.start_time = time.time()

    def process_response(self, request, response):
        log_time = timezone.now()
        if '/admin/' not in request.path:
            self.execution_time = time.time() - self.start_time
            logger = Logger(path=request.path, method=request.method, log_time=log_time,
                            execution_time=self.execution_time)
            logger.save()
        return response


# made global login required
# LoginRequired class which is inherited from MiddlewareMixin:
# class LoginRequiredMiddleware(MiddlewareMixin):
#     """Middleware mixin got 2 methods.
#     process_request -> shouldn't return request, but can return redirect (check our logic before view)
#     process_response -> response (check our logic after view)"""
#
#     def process_request(self, request):
#         exclude_urls = [
#             '/admin/login/',
#             '/login/'
#         ]
#         # check user
#         if not request.user.is_authenticated and request.get_full_path() not in exclude_urls and not request.is_ajax():
#             return redirect('/login/')  # redirect to login page

    # def process_response(self, request, response):
    #     # print(dir(response))
    #     if hasattr(response, 'render'):
    #         response.render()
    #         # response.content = response.content.replace(b'</body>', b'injection</body>')
    #         # response.content = response.content.replace(b'#BANNER_PLACE#', b'<h1>BANNER</h1>')
    #         # response.content = 'Nothing'  # Replace our page content for empty page with "nothing" string
    #     return response




