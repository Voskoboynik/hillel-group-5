from django import forms
from core.models import Group, Teacher, Student, ContactUs


# Group form
class GroupForm(forms.Form):
    course = forms.CharField(max_length=255)
    group_lead = forms.ModelMultipleChoiceField(queryset=Teacher.objects.all())

    def save(self):
        group_lead = self.cleaned_data.pop('group_lead')
        group = Group.objects.create(
            **self.cleaned_data
        )
        group.group_lead.set(group_lead)

    def update_save(self, pk):
        group_lead = self.cleaned_data.pop('group_lead')
        group = Group.objects.get(id=pk)
        group.course = self.cleaned_data['course']
        group.group_lead.set(group_lead)
        group.save()


# Student form:
    # class StudentForm(forms.Form):
    #     name = forms.CharField(max_length=255)
    #     age = forms.IntegerField()
    #     gender = forms.PositiveSmallIntegerField()
    #     course = forms.ModelMultipleChoiceField(queryset=Group.objects.all())
    #     email = forms.EmailField(max_length=40)
    #
    #     def save(self):
    #         course = self.cleaned_data.pop('course')
    #         student = Student.objects.create(
    #             **self.cleaned_data
    #         )
    #         student.course.set(course)

# Student form with Model Usage
class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = "__all__"  # Use: exclude = (<field>) - if need to exclude the required field
        widgets = {
            'gender': forms.widgets.RadioSelect(),
            'course': forms.widgets.CheckboxSelectMultiple()
        }


# Contact us form:
class ContactUsForm(forms.Form):
    name = forms.CharField(max_length=255)
    email = forms.EmailField()
    tittle = forms.CharField(max_length=255)
    message = forms.CharField(widget=forms.widgets.Textarea)

    def save(self):
        ContactUs.objects.create(**self.cleaned_data)