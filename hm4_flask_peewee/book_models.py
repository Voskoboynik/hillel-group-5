from peewee import SqliteDatabase, Model, AutoField, CharField, TextField, ForeignKeyField, IntegerField


db = SqliteDatabase('peewee_books.sqlite3')


class Author(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)

    class Meta:
        database = db


class Genre(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255)

    class Meta:
        database = db


class Books(Model):
    id = AutoField(primary_key=True, unique=True)
    name = CharField(max_length=255, )
    year = IntegerField()
    author = ForeignKeyField(Author, backref='books')
    genre = ForeignKeyField(Genre, backref='books')

    class Meta:
        database = db


if __name__ == '__main__':
    db.create_tables([Author, Genre, Books])