import json
from pprint import pprint
from scrapy.spiders import Spider, Request
from pymongo import MongoClient

client = MongoClient()


# class HotlineSpider(Spider):
#
#     name = 'hotline'
#     user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) ' \
#                  'Chrome/61.0.3163.100 Safari/537.36 '
#     start_urls = [
#         # 'https://hotline.ua/bt/holodilniki/',
#         'https://hotline.ua/bt/zubnye-elektroschetki/'
#     ]
#
#     # collection = client.hotline_db.parsed_fridge
#     collection = client.hotline_db.parsed_elektroschetki
#
#     def link_parse(self, response, **kwargs):
#         data = {
#             'name': response.css('.title__main::text').get().strip(),
#             'price': response.css('.price__value::text').get().replace('\xa0', ''),
#             'specs': []
#         }
#         for spec in response.css('.specifications-list__item::text').getall():
#             data['specs'].append(spec.strip().replace('\n', '').replace(':         ', ':'))
#         self.collection.insert_one(data)
#         yield data
#
#     def parse(self, response, **kwargs):
#         for link in response.css('.item-img-link'):
#             yield Request('https://hotline.ua'+(link.attrib['href']), self.link_parse)
#
#         next_page = response.css('.pagination .pages-nav-next .next::attr(href)').get()
#         if next_page:
#             # yield Request('https://hotline.ua/bt/holodilniki/'+next_page, self.parse)
#             yield Request('https://hotline.ua/bt/zubnye-elektroschetki/'+next_page, self.parse)


class CactusSpider(Spider):

    name = 'Comfy'
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                 'Chrome/61.0.3163.100 Safari/537.36 '
    start_urls = [
        'https://c.ua/ru/naushniki-i-garnitury/',
        'https://c.ua/ru/elektrosamokaty/'
    ]

    collection = client.cactus_db.parsed_goods

    def link_parse(self, response, **kwargs):
        data = {
            'name': response.css('.product-info h2, .product-info h1::text').get(),
            'price': response.css('.overal-price__val span::text').get(),
            'specs': []
        }
        for spec in response.css('.product-info p::text').getall():
            data['specs'].append(spec.strip())
        self.collection.insert_one(data)
        yield data

    def parse(self, response, **kwargs):
        for link in response.css('.product-card'):
            yield Request(link.attrib['href'], self.link_parse)

        next_page = response.css('.pagination>li>a::attr(href)').get()
        if next_page:
            yield Request(next_page, self.parse)
